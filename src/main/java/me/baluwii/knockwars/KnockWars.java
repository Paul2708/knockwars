package me.baluwii.knockwars;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.baluwii.knockwars.commands.SetupCommand;
import me.baluwii.knockwars.eventlisteners.bed.BedListener;
import me.baluwii.knockwars.eventlisteners.player.PlayerJoinListener;
import me.baluwii.knockwars.eventlisteners.player.PlayerLoginListener;
import me.baluwii.knockwars.eventlisteners.player.PlayerQuitListener;
import me.baluwii.knockwars.eventlisteners.player.PlayerSpawnLocationListener;
import me.baluwii.knockwars.eventlisteners.player.block.BlockBreakListener;
import me.baluwii.knockwars.eventlisteners.player.block.BlockPlaceListener;
import me.baluwii.knockwars.eventlisteners.player.chat.PlayerChatListener;
import me.baluwii.knockwars.eventlisteners.player.damage.EntityDamageListener;
import me.baluwii.knockwars.eventlisteners.player.death.PlayerDeathListener;
import me.baluwii.knockwars.eventlisteners.player.death.PlayerRespawnListener;
import me.baluwii.knockwars.eventlisteners.player.food.FoodListener;
import me.baluwii.knockwars.eventlisteners.player.interact.PlayerBedListener;
import me.baluwii.knockwars.eventlisteners.player.inventory.InventoryClickListener;
import me.baluwii.knockwars.eventlisteners.player.move.PlayerMoveListener;
import me.baluwii.knockwars.eventlisteners.world.ThunderChangeListener;
import me.baluwii.knockwars.eventlisteners.world.WeatherChangeListener;
import me.baluwii.knockwars.session.GameSession;
import me.baluwii.knockwars.session.ISession;
import me.baluwii.knockwars.session.map.GameMap;
import me.baluwii.knockwars.session.map.bed.GameBed;
import me.baluwii.knockwars.session.team.GameTeam;
import org.bukkit.Bukkit;
import org.bukkit.WorldBorder;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;

/**
 * This is just a test project to check what the amazon ec2 can handle!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
public class KnockWars extends JavaPlugin {

    private final Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .create();

    private final File mapFile = new File( getDataFolder(), "maps.json" );

    private ISession session;

    @Override
    public void onLoad() {
        try {
            final GameMap gameMap = gson.fromJson( new InputStreamReader( new FileInputStream( mapFile ) ), GameMap.class );
            this.session = new GameSession( gameMap, getDescription().getVersion() );

            for ( final GameBed gameBed : gameMap.getTeamBedMap().values() ) {
                gameBed.setSession( session );
                gameBed.getGameTeam().setGameBed( gameBed );
            }
        } catch ( final FileNotFoundException e ) {
            e.printStackTrace();
        }

        System.out.println( "running version: " + getDescription().getVersion() );
    }

    @Override
    public void onEnable() {
        getServer().getPluginCommand( "setup" ).setExecutor( new SetupCommand( gson, session, mapFile ) );

        getServer().getPluginManager().registerEvents( new BedListener( (GameSession) session ), this );

        getServer().getPluginManager().registerEvents( new BlockBreakListener( (GameSession) session ), this );
        getServer().getPluginManager().registerEvents( new BlockPlaceListener( session ), this );

        getServer().getPluginManager().registerEvents( new PlayerChatListener( session ), this );
        getServer().getPluginManager().registerEvents( new EntityDamageListener( session ), this );

        getServer().getPluginManager().registerEvents( new PlayerBedListener(), this );

        getServer().getPluginManager().registerEvents( new FoodListener(), this );

        getServer().getPluginManager().registerEvents( new InventoryClickListener(), this );

        getServer().getPluginManager().registerEvents( new PlayerLoginListener( (GameSession) session ), this );
        getServer().getPluginManager().registerEvents( new PlayerQuitListener( (GameSession) session ), this );

        getServer().getPluginManager().registerEvents( new PlayerMoveListener( session, session.getGameMap() ), this );

        getServer().getPluginManager().registerEvents( new PlayerJoinListener( session ), this );
        getServer().getPluginManager().registerEvents( new PlayerSpawnLocationListener( session, session.getGameMap() ), this );

        getServer().getPluginManager().registerEvents( new PlayerDeathListener( session ), this );
        getServer().getPluginManager().registerEvents( new PlayerRespawnListener( session, session.getGameMap() ), this );

        getServer().getPluginManager().registerEvents( new ThunderChangeListener(), this );
        getServer().getPluginManager().registerEvents( new WeatherChangeListener(), this );

        if ( this.session.getGameMap().hasBorder() ) {
            final WorldBorder worldBorder = Bukkit.getWorld( "world" ).getWorldBorder();

            worldBorder.setCenter( 0, 0 );
            worldBorder.setSize( this.session.getGameMap().getBorderSize() );

            worldBorder.setDamageAmount( 2 );
            worldBorder.setWarningTime( 15 );
        }

        for ( final Player player : Bukkit.getOnlinePlayers() ) {
            final PlayerLoginEvent event = new PlayerLoginEvent( player );
            Bukkit.getPluginManager().callEvent( event );

            if ( event.getResult() != PlayerLoginEvent.Result.ALLOWED ) {
                player.kickPlayer( event.getKickMessage() );
            }

            Bukkit.getPluginManager().callEvent( new PlayerJoinEvent( player, null ) );

            player.teleport( session.getGameMap().getTeamLocation( session.getPlayer( player ).getGameTeam() ) );
        }

        for ( final GameTeam team : GameTeam.values() ) {
            if ( team.getPlayerList().isEmpty() ) {
                team.setHasBed( false );
                team.getGameBed().destroy( null );
            } else {
                if ( !team.getGameBed().isAlive() )
                    team.getGameBed().revive();
            }
        }

        Bukkit.getScheduler().runTaskTimer( this, session.getBlockScheduler(), 20, 5 );
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}