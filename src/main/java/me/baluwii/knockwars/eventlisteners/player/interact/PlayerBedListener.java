package me.baluwii.knockwars.eventlisteners.player.interact;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
public class PlayerBedListener implements Listener {

    @EventHandler
    public void onBedEnter( final PlayerBedEnterEvent event ) {
        event.setCancelled( true );
    }
}