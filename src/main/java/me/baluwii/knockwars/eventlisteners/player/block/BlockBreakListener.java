package me.baluwii.knockwars.eventlisteners.player.block;

import lombok.RequiredArgsConstructor;
import me.baluwii.knockwars.session.GameSession;
import me.baluwii.knockwars.session.map.bed.IGameBed;
import me.baluwii.knockwars.session.player.IPlayer;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Paul Tristan Wagner <paultristanwagner@gmail.com>
 */
@RequiredArgsConstructor
public class BlockBreakListener implements Listener {

    private final GameSession session;

    @EventHandler
    public void onBlockBreak( final BlockBreakEvent event ) {
        final Block block = event.getBlock();

        if ( block.getType() == Material.BED || block.getType() == Material.BED_BLOCK ) {
            final IPlayer player = session.getPlayer( event.getPlayer() );
            final IGameBed gameBed = session.getGameMap().getTeamBed( event.getBlock().getLocation(), player.getGameTeam() );

            event.setCancelled( true );

            if ( gameBed != null ) {
                gameBed.destroy( player );
            } else {
                event.getPlayer().playSound( event.getBlock().getLocation(), Sound.VILLAGER_NO, 10, 10 );
            }
            return;
        }

        if ( event.getPlayer().isOp() && event.getPlayer().getGameMode().equals( GameMode.CREATIVE ) )
            event.setCancelled( false );
        else
            event.setCancelled( true );

        if ( session.getBlockScheduler().containsLocation( block.getLocation() ) ) {
            block.setType( Material.AIR );
            session.getBlockScheduler().removeLocation( block.getLocation() );

            if ( event.getPlayer().getGameMode().equals( GameMode.SURVIVAL ) )
                block.getWorld().dropItem( block.getLocation().clone(), new ItemStack( Material.SANDSTONE, 1 ) );
        }
    }
}
