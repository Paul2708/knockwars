package me.baluwii.knockwars.eventlisteners.player.block;

import lombok.RequiredArgsConstructor;
import me.baluwii.knockwars.session.ISession;
import me.baluwii.knockwars.session.player.IPlayer;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.material.MaterialData;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Paul Tristan Wagner <paultristanwagner@gmail.com>
 */
@RequiredArgsConstructor
public class BlockPlaceListener implements Listener {

    private final ISession session;

    @EventHandler
    public void onBlockPlace( final BlockPlaceEvent event ) {
        final Block block = event.getBlock();
        final IPlayer player = session.getPlayer( event.getPlayer() );

        session.getBlockScheduler().addBlock( player, block.getLocation(), new MaterialData( block.getType(), block.getData() ) );
    }
}