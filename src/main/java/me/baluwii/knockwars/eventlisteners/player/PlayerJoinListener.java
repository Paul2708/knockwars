package me.baluwii.knockwars.eventlisteners.player;

import lombok.RequiredArgsConstructor;
import me.baluwii.knockwars.session.ISession;
import me.baluwii.knockwars.session.player.IPlayer;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@RequiredArgsConstructor
public class PlayerJoinListener implements Listener {

    private final ISession session;

    @EventHandler
    public void onPlayerJoin( final PlayerJoinEvent event ) {
        event.setJoinMessage( null );

        final IPlayer player = session.getPlayer( event.getPlayer() );

        player.getPlayer().setScoreboard( player.getScoreboard().getScoreboard() );

        if ( event.getPlayer().isDead() ) {
            event.getPlayer().spigot().respawn();
        }

        event.getPlayer().getInventory().setChestplate( getLeatherChestplate( player ) );

        player.updateInventory();

        event.getPlayer().sendMessage( "§6Dies ist nur ein Test-Server, um die Amazon-Cloud zu testen!" );
        event.getPlayer().sendMessage( "§6Mehr Infos: §chttps://discord.gg/FRmH7w6" );
    }

    private ItemStack getLeatherChestplate( IPlayer player ) {
        final ItemStack itemStack = new ItemStack( Material.LEATHER_CHESTPLATE, 1 );
        final LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) itemStack.getItemMeta();

        leatherArmorMeta.setColor( player.getGameTeam().getColor() );
        leatherArmorMeta.spigot().setUnbreakable( true );

        itemStack.setItemMeta( leatherArmorMeta );
        return itemStack;
    }
}