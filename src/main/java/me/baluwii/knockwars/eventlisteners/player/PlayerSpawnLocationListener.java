package me.baluwii.knockwars.eventlisteners.player;

import lombok.RequiredArgsConstructor;
import me.baluwii.knockwars.session.ISession;
import me.baluwii.knockwars.session.map.IGameMap;
import me.baluwii.knockwars.session.team.GameTeam;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.spigotmc.event.player.PlayerSpawnLocationEvent;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@RequiredArgsConstructor
public class PlayerSpawnLocationListener implements Listener {

    private final ISession session;
    private final IGameMap gameMap;

    @EventHandler
    public void onPlayerSpawnLoc( final PlayerSpawnLocationEvent event ) {
        final GameTeam gameTeam = session.getPlayer( event.getPlayer() ).getGameTeam();
        event.setSpawnLocation( gameMap.getTeamLocation( gameTeam ) );
    }
}