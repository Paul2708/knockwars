package me.baluwii.knockwars.eventlisteners.player.death;

import lombok.RequiredArgsConstructor;
import me.baluwii.knockwars.session.ISession;
import me.baluwii.knockwars.session.map.IGameMap;
import me.baluwii.knockwars.session.player.IPlayer;
import me.baluwii.knockwars.session.team.GameTeam;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@RequiredArgsConstructor
public class PlayerRespawnListener implements Listener {

    private final ISession session;
    private final IGameMap gameMap;

    @EventHandler
    public void onPlayerRespawn( final PlayerRespawnEvent event ) {
        final IPlayer player = session.getPlayer( event.getPlayer() );

        final GameTeam gameTeam = player.getGameTeam();
        event.setRespawnLocation( gameMap.getTeamLocation( gameTeam ) );

        event.getPlayer().getInventory().setChestplate( getLeatherChestplate( player ) );

        player.updateInventory();
    }

    private ItemStack getLeatherChestplate(IPlayer player ) {
        final ItemStack itemStack = new ItemStack( Material.LEATHER_CHESTPLATE, 1 );
        final LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) itemStack.getItemMeta();

        leatherArmorMeta.setColor( player.getGameTeam().getColor() );
        leatherArmorMeta.spigot().setUnbreakable( true );

        itemStack.setItemMeta( leatherArmorMeta );
        return itemStack;
    }
}