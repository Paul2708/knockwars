package me.baluwii.knockwars.eventlisteners.player;

import lombok.RequiredArgsConstructor;
import me.baluwii.knockwars.session.GameSession;
import me.baluwii.knockwars.session.player.IPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@RequiredArgsConstructor
public class PlayerQuitListener implements Listener {

    private final GameSession session;

    @EventHandler
    public void onPlayerQuit( final PlayerQuitEvent event ) {
        event.setQuitMessage( null );

        final IPlayer player = session.getPlayer( event.getPlayer() );
        session.getPlayerMap().remove( event.getPlayer() );

        player.leaveTeam();

        for ( final IPlayer onlinePlayer : session.getPlayerMap().values() ) {
            onlinePlayer.getScoreboard().updateTeam( player.getGameTeam() );
            onlinePlayer.getScoreboard().updateState( player.getGameTeam(), player.getGameTeam().isAlive() );
        }

        session.getBlockScheduler().resetByPlayer( player );
    }
}