package me.baluwii.knockwars.eventlisteners.player;

import lombok.RequiredArgsConstructor;
import me.baluwii.knockwars.session.GameSession;
import me.baluwii.knockwars.session.player.GamePlayer;
import me.baluwii.knockwars.session.player.IPlayer;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@RequiredArgsConstructor
public class PlayerLoginListener implements Listener {

    private final GameSession session;

    @EventHandler( ignoreCancelled = true, priority = EventPriority.HIGHEST )
    public void onPlayerLogin( final PlayerLoginEvent event ) {
        if ( Bukkit.getServer().hasWhitelist() && !event.getPlayer().isWhitelisted() ) {
            event.disallow( PlayerLoginEvent.Result.KICK_WHITELIST, "Du stehst nicht auf der Whitelist!" );
        } else if ( Bukkit.getServer().getOnlinePlayers().size() >= Bukkit.getServer().getMaxPlayers()
                && !event.getPlayer().hasPermission( "knockwars.join_full" ) ) {
            event.disallow( PlayerLoginEvent.Result.KICK_FULL, "Der Server ist voll!" );
        } else {
            final GamePlayer gamePlayer = new GamePlayer( event.getPlayer() );
            session.getPlayerMap().put( event.getPlayer(), gamePlayer );

            event.allow();

            for ( final IPlayer onlinePlayer : session.getPlayerMap().values() ) {
                onlinePlayer.getScoreboard().addPlayer( gamePlayer );
                gamePlayer.getScoreboard().addPlayer( onlinePlayer );

                onlinePlayer.getScoreboard().updateState( gamePlayer.getGameTeam(), gamePlayer.getGameTeam().isAlive() );
                onlinePlayer.getScoreboard().updateTeam( gamePlayer.getGameTeam() );
            }
        }
    }
}