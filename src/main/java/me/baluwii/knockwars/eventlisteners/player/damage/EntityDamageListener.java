package me.baluwii.knockwars.eventlisteners.player.damage;

import lombok.RequiredArgsConstructor;
import me.baluwii.knockwars.session.ISession;
import me.baluwii.knockwars.session.player.IPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@RequiredArgsConstructor
public class EntityDamageListener implements Listener {

    private final ISession session;

    @EventHandler
    public void onEntityDamage( final EntityDamageEvent event ) {
        if ( !( event.getEntity() instanceof Player ) )
            return;

        final IPlayer player = session.getPlayer( (Player) event.getEntity() );

        if ( player.getGameTeam() == null ) {
            event.setCancelled( true );
            return;
        }

        if ( event.getCause() == EntityDamageEvent.DamageCause.FALL ) {
            if ( event.getEntity().getLocation().distanceSquared( session.getGameMap().getTeamLocation( player.getGameTeam() ) ) < 8 ) {
                event.setCancelled( true );
            }
        }

        player.getPlayer().getInventory().getChestplate().setDurability( (short) 0 );
    }

    @EventHandler
    public void onEntityDamageByEntity( final EntityDamageByEntityEvent event ) {
        if ( event.getEntity() instanceof Player
                && event.getDamager() instanceof Player ) {
            final IPlayer playerA = session.getPlayer( (Player) event.getEntity() ),
                    playerB = session.getPlayer( (Player) event.getDamager() );

            if ( playerA.getGameTeam().equals( playerB.getGameTeam() ) ) {
                event.setCancelled( true );
            }
        }
    }
}