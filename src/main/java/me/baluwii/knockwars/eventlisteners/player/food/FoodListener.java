package me.baluwii.knockwars.eventlisteners.player.food;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
public class FoodListener implements Listener {

    @EventHandler
    public void onFoodLevelChange( final FoodLevelChangeEvent event ) {
        event.setCancelled( true );
    }
}