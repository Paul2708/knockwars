package me.baluwii.knockwars.eventlisteners.player.chat;

import lombok.RequiredArgsConstructor;
import me.baluwii.knockwars.session.ISession;
import me.baluwii.knockwars.session.player.IPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@RequiredArgsConstructor
public class PlayerChatListener implements Listener {

    private final ISession session;

    @EventHandler
    public void onPlayerChat( final AsyncPlayerChatEvent event ) {
        final IPlayer player = session.getPlayer( event.getPlayer() );
        event.setMessage( event.getMessage().replace( "%", "%%" ) );

        if ( event.getMessage().startsWith( "@team " ) ) {
            event.setCancelled( true );

            for ( final Player teamPlayer : player.getGameTeam().getPlayerList() ) {
                teamPlayer.sendMessage( "§7[Team] " + player.getGameTeam().getColorCode() + event.getPlayer().getDisplayName() + "§7: " + event.getMessage().replaceFirst( "@team ", "" ) );
            }
        } else {
            event.setFormat( "§7[@all] " + player.getGameTeam().getColorCode() + event.getPlayer().getDisplayName() + "§7: " + event.getMessage().replaceFirst( "@all ", "" ) );
        }
    }
}
