package me.baluwii.knockwars.eventlisteners.player.inventory;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * This listener disables armor dropping.
 * Created: 22.08.2018
 *
 * @author Paul2708 (playerpaul2708@gmx.de)
 */
public class InventoryClickListener implements Listener {

    /**
     * Cancel the click event, if the clicked item is a {@link Material#LEATHER_CHESTPLATE}.
     *
     * @param event inventory click event
     */
    @EventHandler
    public void onInventoryClick( final InventoryClickEvent event ) {
        final ItemStack clickedItem = event.getCurrentItem();

        if ( clickedItem != null && clickedItem.getType() == Material.LEATHER_CHESTPLATE ) {
            event.setCancelled( true );
        }
    }
}