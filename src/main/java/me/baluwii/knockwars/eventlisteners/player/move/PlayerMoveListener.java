package me.baluwii.knockwars.eventlisteners.player.move;

import lombok.RequiredArgsConstructor;
import me.baluwii.knockwars.session.ISession;
import me.baluwii.knockwars.session.map.IGameMap;
import me.baluwii.knockwars.session.player.IPlayer;
import me.baluwii.knockwars.session.team.GameTeam;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@RequiredArgsConstructor
public class PlayerMoveListener implements Listener {

    private final ISession session;
    private final IGameMap gameMap;

    @EventHandler
    public void onPlayerMove( final PlayerMoveEvent event ) {
        if ( !event.getPlayer().getGameMode().equals( GameMode.SURVIVAL ) )
            return;

        if ( event.getTo().getY() <= gameMap.getMinHeightY() ) {
            final IPlayer player = session.getPlayer( event.getPlayer() );
            final GameTeam gameTeam = player.getGameTeam();

            event.getPlayer().setFallDistance( 0f );
            event.setTo( gameMap.getTeamLocation( gameTeam ) );

            session.getBlockScheduler().resetByPlayer( player );

            player.updateInventory();
        }
    }
}