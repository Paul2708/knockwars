package me.baluwii.knockwars.eventlisteners.player.death;

import lombok.RequiredArgsConstructor;
import me.baluwii.knockwars.session.ISession;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@RequiredArgsConstructor
public class PlayerDeathListener implements Listener {

    private final ISession session;

    @EventHandler
    public void onDeath( final PlayerDeathEvent event ) {
        event.setDeathMessage( null );
        event.getDrops().clear();

        session.getBlockScheduler().resetByPlayer( session.getPlayer( event.getEntity() ) );
    }
}