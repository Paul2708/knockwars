package me.baluwii.knockwars.eventlisteners.world;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.ThunderChangeEvent;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
public class ThunderChangeListener implements Listener {

    @EventHandler
    public void onThunderChange( final ThunderChangeEvent event ) {
        event.setCancelled( true );
    }
}