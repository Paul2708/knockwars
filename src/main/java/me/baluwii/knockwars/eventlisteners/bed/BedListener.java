package me.baluwii.knockwars.eventlisteners.bed;

import lombok.RequiredArgsConstructor;
import me.baluwii.knockwars.events.BedDestroyEvent;
import me.baluwii.knockwars.events.BedReviveEvent;
import me.baluwii.knockwars.session.GameSession;
import me.baluwii.knockwars.session.player.IPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.util.Vector;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@RequiredArgsConstructor
public class BedListener implements Listener {

    private final GameSession session;

    @EventHandler
    public void onBedDestroy( final BedDestroyEvent event ) {
        Bukkit.broadcastMessage( "§6Das Bett von Team " + event.getGameTeam().getColorCode() + event.getGameTeam().name() + "§6 wurde von " + event.getPlayer().getGameTeam().getColorCode() + event.getPlayer().getPlayer().getDisplayName() + " §6abgebaut!" );

        event.getGameTeam().setHasBed( false );

        for ( final IPlayer player : session.getPlayerMap().values() ) {
            player.getScoreboard().updateState( event.getGameTeam(), false );
        }

        for ( final Player o : event.getGameTeam().getPlayerList() ) {
            final IPlayer player = session.getPlayer( o );

            player.setSpectator( true );
            session.getBlockScheduler().resetByPlayer( player );
        }
    }

    @EventHandler
    public void onBedRevive( final BedReviveEvent event ) {
        Bukkit.broadcastMessage( "§6Das Bett von Team " + event.getGameTeam().getColorCode() + event.getGameTeam().name() + " §6ist respawnt!" );

        event.getGameTeam().setHasBed( true );

        for ( final IPlayer player : session.getPlayerMap().values() ) {
            player.getScoreboard().updateState( event.getGameTeam(), event.getGameTeam().isAlive() );
        }

        final Location location = session.getGameMap().getTeamLocation( event.getGameTeam() );

        location.getWorld().playEffect( location, Effect.SPLASH, 5, 5 );

        for ( final Entity entity : location.getWorld().getNearbyEntities( location, 8, 8, 8 ) ) {
            if ( !( entity instanceof Player ) )
                continue;

            final IPlayer player = session.getPlayer( (Player) entity );

            if ( player.getGameTeam() == null || player.getGameTeam().equals( event.getGameTeam() ) )
                continue;

            final Vector vector = player.getPlayer().getLocation().toVector().subtract( location.toVector() );
            player.getPlayer().setVelocity( vector.multiply( 3D ) );

            player.getPlayer().sendMessage( "§6SPLASH! Geh weg von toten Betten, um Spielern die Chance zu geben, mit zu spielen!" );
        }

        for ( final Player o : event.getGameTeam().getPlayerList() ) {
            final IPlayer player = session.getPlayer( o );

            player.setSpectator( false );
            o.teleport( location );
        }
    }
}