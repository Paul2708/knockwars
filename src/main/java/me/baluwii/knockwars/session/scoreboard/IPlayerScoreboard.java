package me.baluwii.knockwars.session.scoreboard;

import me.baluwii.knockwars.session.player.IPlayer;
import me.baluwii.knockwars.session.team.GameTeam;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
public interface IPlayerScoreboard {

    Player getPlayer();

    Scoreboard getScoreboard();

    void addPlayer( IPlayer gamePlayer );

    void updateTeam( final GameTeam gameTeam );

    void updateState( final GameTeam gameTeam, final boolean alive );
}