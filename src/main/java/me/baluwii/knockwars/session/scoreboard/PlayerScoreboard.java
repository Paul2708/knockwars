package me.baluwii.knockwars.session.scoreboard;

import lombok.Getter;
import me.baluwii.knockwars.session.player.IPlayer;
import me.baluwii.knockwars.session.team.GameTeam;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@Getter
public class PlayerScoreboard implements IPlayerScoreboard {

    private final Player player;
    private final Scoreboard scoreboard;

    private final Objective objective;

    public PlayerScoreboard( final Player player ) {
        this.player = player;
        this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

        // Register a new objective.
        objective = scoreboard.registerNewObjective( "sidebar", "dummy" );

        // Setup the display slot and name.
        objective.setDisplaySlot( DisplaySlot.SIDEBAR );
        objective.setDisplayName( "§aKnockWars §8➟ §eEC2 AWS" );

        // Display the server ip with a spacer:
        objective.getScore( "§a" ).setScore( 19 );
        objective.getScore( "§7Server:" ).setScore( 18 );
        objective.getScore( "§a52.28.217.168" ).setScore( 17 );

        // Display the teams:
        objective.getScore( "§f" ).setScore( 16 );
        objective.getScore( "§7Teams:" ).setScore( 15 );

        for ( final GameTeam gameTeam : GameTeam.values() ) {
            final Team team = scoreboard.registerNewTeam( gameTeam.name() );

            team.setPrefix( gameTeam.getColorCode() + "⬛ " );
            team.setNameTagVisibility( NameTagVisibility.HIDE_FOR_OWN_TEAM );

            objective.getScore( gameTeam.getKey() ).setScore( gameTeam.getPlayerList().size() );

            final Team keyTeam = scoreboard.registerNewTeam( gameTeam.name() + "_score" );
            keyTeam.addEntry( gameTeam.getKey() );

            keyTeam.setSuffix( " " + gameTeam.getColorCode() + gameTeam.name() );
            updateState( gameTeam, gameTeam.isAlive() );
        }
    }

    @Override
    public void addPlayer( final IPlayer player ) {
        scoreboard.getTeam( player.getGameTeam().name() ).addEntry( player.getPlayer().getName() );
    }

    @Override
    public void updateTeam( final GameTeam gameTeam ) {
        objective.getScore( gameTeam.getKey() ).setScore( gameTeam.getPlayerList().size() );
    }

    @Override
    public void updateState( final GameTeam gameTeam, final boolean alive ) {
        scoreboard.getTeam( gameTeam.name() + "_score" )
                .setPrefix( gameTeam.getColorCode() + ( alive ? "§a✔" : "§c✗" ) );
    }
}