package me.baluwii.knockwars.session;

import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import me.baluwii.knockwars.session.map.IGameMap;
import me.baluwii.knockwars.session.map.blocks.BlockScheduler;
import me.baluwii.knockwars.session.player.IPlayer;
import me.baluwii.knockwars.session.team.GameTeam;
import org.bukkit.entity.Player;

import java.util.Map;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@RequiredArgsConstructor
public class GameSession implements ISession {

    @Getter
    private final IGameMap gameMap;

    @Getter
    private final String pluginVersion;

    @Getter
    private final BlockScheduler blockScheduler = new BlockScheduler();

    @Getter
    private final Map<Player, IPlayer> playerMap = Maps.newConcurrentMap();

    @Override
    public IPlayer getPlayer( final Player player ) {
        return playerMap.get( player );
    }

    public void reviveBed( final GameTeam team ) {
        for ( final IPlayer player : playerMap.values() ) {
            player.getScoreboard().updateState( team, true );
        }
    }

    public void destroyBed( final GameTeam team, final IPlayer destroyer ) {
        for ( final IPlayer player : playerMap.values() ) {
            player.getScoreboard().updateState( team, false );
        }
    }
}