package me.baluwii.knockwars.session.team;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import me.baluwii.knockwars.session.map.bed.IGameBed;
import org.bukkit.Color;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@Getter
@RequiredArgsConstructor
public enum GameTeam {

    BLUE( Color.BLUE, "§9", "§x" ),
    RED( Color.RED, "§c", "§y" ),
    GREEN( Color.GREEN, "§a", "§p" ),
    YELLOW( Color.YELLOW, "§e", "§j" );

    private final Color color;
    private final String colorCode;

    private final String key;

    private final List<Player> playerList = Lists.newArrayList();

    @Setter
    private IGameBed gameBed;

    @Setter
    private boolean hasBed = false;

    public boolean isAlive() {
        return ( !playerList.isEmpty() && hasBed );
    }

    public static GameTeam getRandomTeam() {
        final List<GameTeam> gameTeams = Lists.newArrayList( values() );
        Collections.shuffle( gameTeams );

        return gameTeams.stream()
                .sorted( Comparator.comparingInt( o -> o.getPlayerList().size() ) )
                .collect( Collectors.toList() ).get( 0 );
    }
}