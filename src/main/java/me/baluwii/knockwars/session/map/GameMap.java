package me.baluwii.knockwars.session.map;

import com.google.common.collect.Maps;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import me.baluwii.knockwars.session.map.bed.GameBed;
import me.baluwii.knockwars.session.map.serialization.SerializableLocation;
import me.baluwii.knockwars.session.team.GameTeam;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.Map;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@Getter
public class GameMap implements IGameMap {

    @SerializedName( "team_locations" )
    private final Map<GameTeam, SerializableLocation> teamLocationMap = Maps.newConcurrentMap();

    @SerializedName( "team_beds" )
    private final Map<GameTeam, GameBed> teamBedMap = Maps.newConcurrentMap();

    @SerializedName( "enable_border" )
    private boolean border = true;
    @SerializedName( "border_size" )
    private double borderSize = 50;

    @SerializedName( "min_height_y" )
    private double minHeightY = 50.0;

    @Override
    public Location getTeamLocation( final GameTeam team ) {
        return !teamLocationMap.containsKey( team ) ? Bukkit.getWorld( "world" ).getSpawnLocation() : teamLocationMap.get( team ).asSpigotLocation();
    }

    @Override
    public GameBed getTeamBed( final GameTeam team ) {
        return teamBedMap.getOrDefault( team, null );
    }

    @Override
    public GameBed getTeamBed( final Location location, final GameTeam team ) {
        for ( final Map.Entry<GameTeam, GameBed> entry : teamBedMap.entrySet() ) {
            if ( entry.getKey() == team )
                continue;
            if ( entry.getValue().hasLocation( location ) )
                return entry.getValue();
        }
        return null;
    }

    @Override
    public boolean hasBorder() {
        return border;
    }

    @Override
    public double getBorderSize() {
        return borderSize;
    }
}