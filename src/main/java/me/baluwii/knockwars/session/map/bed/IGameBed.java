package me.baluwii.knockwars.session.map.bed;

import me.baluwii.knockwars.session.player.IPlayer;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Paul Tristan Wagner <paultristanwagner@gmail.com>
 */
public interface IGameBed {

    boolean isAlive();

    void destroy( final IPlayer player );

    void revive();
}
