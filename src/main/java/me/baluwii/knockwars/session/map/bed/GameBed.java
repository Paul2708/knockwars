package me.baluwii.knockwars.session.map.bed;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import me.baluwii.knockwars.KnockWars;
import me.baluwii.knockwars.events.BedDestroyEvent;
import me.baluwii.knockwars.events.BedReviveEvent;
import me.baluwii.knockwars.session.ISession;
import me.baluwii.knockwars.session.map.serialization.SerializableLocation;
import me.baluwii.knockwars.session.player.IPlayer;
import me.baluwii.knockwars.session.team.GameTeam;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.material.Bed;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Paul Tristan Wagner <paultristanwagner@gmail.com>
 */
@RequiredArgsConstructor
@Getter
public class GameBed implements IGameBed {

    @Setter
    private transient ISession session;

    @SerializedName( "game_team" )
    private final GameTeam gameTeam;

    @SerializedName( "bed_face" )
    private final BlockFace blockFace;
    @SerializedName( "bed_head_location" )
    private final SerializableLocation headLocation;

    @Getter
    private transient boolean alive = true;

    @Override
    public void destroy( final IPlayer player ) {
        alive = false;

        final Block bedHeadBlock = headLocation.asSpigotLocation().getBlock();
        final Block bedFootBlock = bedHeadBlock.getRelative( blockFace.getOppositeFace() );

        bedFootBlock.setType( Material.AIR );
        bedHeadBlock.setType( Material.AIR );

        if ( player != null ) {
            Bukkit.getScheduler().runTaskLater( JavaPlugin.getPlugin( KnockWars.class ), () -> {
                if ( !gameTeam.getPlayerList().isEmpty() )
                    revive();
            }, 10 * 20L );
            Bukkit.getServer().getPluginManager().callEvent( new BedDestroyEvent( this, gameTeam, player ) );
        }
    }

    @Override
    public void revive() {
        final Block bedHeadBlock = headLocation.asSpigotLocation().getBlock(),
                bedFootBlock = bedHeadBlock.getRelative( blockFace.getOppositeFace() );

        BlockState bedFootState = bedFootBlock.getState();
        bedFootState.setType( Material.BED_BLOCK );
        Bed bedFootData = new Bed();
        bedFootData.setHeadOfBed( false );
        bedFootData.setFacingDirection( blockFace );
        bedFootState.setData( bedFootData );
        bedFootState.update( true );

        BlockState bedHeadState = bedHeadBlock.getState();
        bedHeadState.setType( Material.BED_BLOCK );
        Bed bedHeadData = new Bed();
        bedHeadData.setHeadOfBed( true );
        bedHeadData.setFacingDirection( blockFace );
        bedHeadState.setData( bedHeadData );
        bedHeadState.update( true );

        alive = true;

        Bukkit.getServer().getPluginManager().callEvent( new BedReviveEvent( this, gameTeam ) );
    }

    public boolean hasLocation( final Location location ) {
        Location bedHeadLocation = headLocation.asSpigotLocation();
        if ( bedHeadLocation.equals( location ) )
            return true;
        Location bedFootLocation = bedHeadLocation.getBlock().getRelative( blockFace.getOppositeFace() ).getLocation();
        return bedFootLocation.equals( location );
    }
}
