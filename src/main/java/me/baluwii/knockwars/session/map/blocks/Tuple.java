package me.baluwii.knockwars.session.map.blocks;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
class Tuple<X, Y> {

    private final X x;
    private final Y y;

}