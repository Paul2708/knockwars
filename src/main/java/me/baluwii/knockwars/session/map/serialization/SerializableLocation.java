package me.baluwii.knockwars.session.map.serialization;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * JavaDoc this file!
 * Created: 22.07.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class SerializableLocation {

    @SerializedName( "world_name" )
    private String worldName = null;

    private double x, y, z;
    private float yaw, pitch;

    public static SerializableLocation getSerializedLocation( final Location location ) {
        return new SerializableLocation( location.getWorld().getName(), location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch() );
    }

    public Location asSpigotLocation() {
        return new Location( Bukkit.getWorld( worldName ), x, y, z, yaw, pitch );
    }
}