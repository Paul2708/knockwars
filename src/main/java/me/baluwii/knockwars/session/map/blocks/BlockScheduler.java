package me.baluwii.knockwars.session.map.blocks;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import me.baluwii.knockwars.session.player.IPlayer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.material.MaterialData;

import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
public class BlockScheduler implements Runnable {

    private final static int limit = 20;

    private final Map<IPlayer, List<Tuple<Location, MaterialData>>> blockCacheMap = Maps.newConcurrentMap();

    private final Map<Location, TupleData> locationPlayerMap = Maps.newConcurrentMap();
    private final Map<Location, BlockInformation> blockInformationMap = Maps.newConcurrentMap();

    private final BlockingQueue<Tuple<Location, MaterialData>> queue = Queues.newLinkedBlockingDeque();
    private final List<BlockInformation> blockLists = Lists.newArrayList();

    @Override
    public void run() {
        while ( !queue.isEmpty()
                && blockLists.size() < limit ) {

            final BlockInformation blockInformation = new BlockInformation( queue.remove() );

            blockLists.add( blockInformation );
            blockInformationMap.put( blockInformation.tuple.getX(), blockInformation );
        }

        final List<BlockInformation> toRemove = Lists.newArrayList();

        for ( BlockInformation block : blockLists ) {
            final BlockState blockState = block.tuple.getX().getBlock().getState();
            final Material material = block.tuple.getX().getBlock().getType();

            if ( material == Material.BED
                    || material == Material.BED_BLOCK ) {
                toRemove.add( block );
                continue;
            }

            if ( block.stage == 2 ) {
                blockState.setType( Material.AIR );
                toRemove.add( block );
            } else {
                final byte subId = ( block.stage == 0 ? (byte) 4 : (byte) 6 );

                blockState.setType( Material.STAINED_CLAY );
                blockState.setData( new MaterialData( Material.STAINED_CLAY, subId ) );

                block.stage += 1;
            }

            blockState.update( true, false );
        }

        blockLists.removeAll( toRemove );
    }

    public boolean containsLocation( final Location location ) {
        return locationPlayerMap.containsKey( location );
    }

    /**
     * Method to reset all blocks placed by a player.
     *
     * @param player
     */
    public void resetByPlayer( final IPlayer player ) {
        if ( blockCacheMap.containsKey( player ) ) {
            queue.addAll( blockCacheMap.get( player ) );
            blockCacheMap.remove( player );
        }
    }

    /**
     * Let a player place a block.
     *
     * @param player
     * @param location
     * @param materialData
     */
    public void addBlock( final IPlayer player, final Location location, final MaterialData materialData ) {
        final List<Tuple<Location, MaterialData>> playerBlockCache = blockCacheMap.getOrDefault( player, Lists.newArrayList() );
        final Tuple<Location, MaterialData> tuple = new Tuple<>( location, materialData );
        playerBlockCache.add( tuple );

        blockCacheMap.put( player, playerBlockCache );
        locationPlayerMap.put( location, new TupleData( player, tuple ) );
    }

    /**
     * Remove a specified location.
     *
     * @param location
     */
    public void removeLocation( final Location location ) {
        final TupleData tupleData = locationPlayerMap.get( location );
        final IPlayer player = tupleData.getPlayer();
        locationPlayerMap.remove( location );

        if ( blockCacheMap.containsKey( player ) ) {
            blockCacheMap.get( player ).remove( tupleData.getTuple() );

            if ( blockCacheMap.get( player ).isEmpty() )
                blockCacheMap.remove( player );
        }

        if ( blockInformationMap.containsKey( location ) ) {
            final BlockInformation blockInformation = blockInformationMap.get( location );

            blockInformationMap.remove( location );
            blockLists.remove( blockInformation );
        }
    }

    @RequiredArgsConstructor
    private class BlockInformation {

        private int stage = 0;
        private final Tuple<Location, MaterialData> tuple;
    }

    @RequiredArgsConstructor
    @Getter
    private class TupleData {

        private final IPlayer player;
        private final Tuple<Location, MaterialData> tuple;
    }
}