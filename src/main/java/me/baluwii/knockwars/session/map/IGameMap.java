package me.baluwii.knockwars.session.map;

import me.baluwii.knockwars.session.map.bed.GameBed;
import me.baluwii.knockwars.session.map.bed.IGameBed;
import me.baluwii.knockwars.session.team.GameTeam;
import org.bukkit.Location;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
public interface IGameMap {

    Location getTeamLocation( final GameTeam team );

    GameBed getTeamBed( final GameTeam team );

    IGameBed getTeamBed( final Location location, final GameTeam team );

    double getMinHeightY();

    boolean hasBorder();

    double getBorderSize();
}