package me.baluwii.knockwars.session.player;

import me.baluwii.knockwars.session.scoreboard.IPlayerScoreboard;
import me.baluwii.knockwars.session.team.GameTeam;
import org.bukkit.entity.Player;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
public interface IPlayer {

    Player getPlayer();

    GameTeam getGameTeam();

    IPlayerScoreboard getScoreboard();

    void joinTeam( final GameTeam gameTeam );

    void leaveTeam();

    void updateInventory();

    boolean isSpectator();

    void setSpectator( final boolean spectator );
}