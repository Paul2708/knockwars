package me.baluwii.knockwars.session.player;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import me.baluwii.knockwars.session.scoreboard.IPlayerScoreboard;
import me.baluwii.knockwars.session.scoreboard.PlayerScoreboard;
import me.baluwii.knockwars.session.team.GameTeam;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@Getter
public class GamePlayer implements IPlayer {

    private final static List<ItemStack> itemStackList = Lists.newArrayList( Arrays.asList(
            getKnockbackStick(),
            new ItemStack( Material.WOOD_SWORD, 1 ),
            new ItemStack( Material.WOOD_PICKAXE, 1 ),
            null,
            new ItemStack( Material.SANDSTONE, 64 ),
            new ItemStack( Material.SANDSTONE, 64 )
    ) );

    private static ItemStack getKnockbackStick() {
        final ItemStack itemStack = new ItemStack( Material.STICK, 1 );
        itemStack.addUnsafeEnchantment( Enchantment.KNOCKBACK, 2 );

        return itemStack;
    }

    private final Player player;
    private final IPlayerScoreboard scoreboard;

    private Material blockMaterial = Material.SANDSTONE;

    @Setter
    private GameTeam gameTeam;

    public GamePlayer( final Player player ) {
        this.player = player;
        this.scoreboard = new PlayerScoreboard( player );

        joinTeam( GameTeam.getRandomTeam() );
    }

    @Override
    public void joinTeam( final GameTeam gameTeam ) {
        if ( this.gameTeam != null ) {
            leaveTeam();
        }

        this.gameTeam = gameTeam;

        if ( this.gameTeam.getPlayerList().isEmpty() && !this.gameTeam.isHasBed() ) {
            this.gameTeam.setHasBed( true );
            this.gameTeam.getGameBed().revive();
        }

        gameTeam.getPlayerList().add( player );
    }

    @Override
    public void leaveTeam() {
        this.gameTeam.getPlayerList().remove( player );

        if ( this.gameTeam.getPlayerList().isEmpty() ) {
            this.gameTeam.setHasBed( false );
            this.gameTeam.getGameBed().destroy( null );
        }
    }

    @Override
    public void updateInventory() {
        player.setAllowFlight( false );
        player.setFlying( false );

        player.getInventory().clear();

        for ( int index = 0; index < itemStackList.size(); index++ ) {
            final ItemStack itemStack = itemStackList.get( index );

            if ( itemStack == null )
                continue;

            if ( itemStack.getType() == Material.SANDSTONE
                    && blockMaterial != Material.SANDSTONE )
                itemStack.setType( blockMaterial );

            player.getInventory().setItem( index, itemStack );
        }

        player.updateInventory();

        player.setHealth( player.getMaxHealth() );
        player.setFoodLevel( 20 );

        player.setGameMode( GameMode.SURVIVAL );
    }

    @Override
    public boolean isSpectator() {
        return gameTeam == null || !gameTeam.isHasBed();
    }

    @Override
    public void setSpectator( boolean spectator ) {
        if ( !spectator ) {
            updateInventory();
        } else {
            player.setAllowFlight( true );
            player.setGameMode( GameMode.SPECTATOR );
        }
    }
}