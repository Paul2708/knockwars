package me.baluwii.knockwars.session;

import me.baluwii.knockwars.session.map.IGameMap;
import me.baluwii.knockwars.session.map.blocks.BlockScheduler;
import me.baluwii.knockwars.session.player.IPlayer;
import org.bukkit.entity.Player;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
public interface ISession {

    String getPluginVersion();

    BlockScheduler getBlockScheduler();

    IGameMap getGameMap();

    IPlayer getPlayer( final Player player );
}