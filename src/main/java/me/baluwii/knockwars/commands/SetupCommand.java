package me.baluwii.knockwars.commands;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import me.baluwii.knockwars.session.ISession;
import me.baluwii.knockwars.session.map.bed.GameBed;
import me.baluwii.knockwars.session.map.GameMap;
import me.baluwii.knockwars.session.map.serialization.SerializableLocation;
import me.baluwii.knockwars.session.team.GameTeam;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@RequiredArgsConstructor
public class SetupCommand implements CommandExecutor {

    private final Gson gson;
    private final ISession session;
    private final File mapFile;

    @Override
    public boolean onCommand( CommandSender commandSender, Command command, String s, String[] args ) {
        if ( args[0].equalsIgnoreCase( "save" ) ) {
            try {
                final FileWriter fileWriter = new FileWriter( mapFile );
                fileWriter.write( gson.toJson( session.getGameMap() ) );
                fileWriter.close();

                commandSender.sendMessage( "Saved." );
            } catch ( IOException e ) {
                e.printStackTrace();

                commandSender.sendMessage( "Error while saving!" );
            }

            return true;
        } else if ( args[0].equalsIgnoreCase( "setSpawn" ) ) {
            if ( args.length != 2 ) {
                commandSender.sendMessage( "Korrekte Nutzung: /setup setSpawn <Team>" );
                return true;
            }

            final GameTeam gameTeam = GameTeam.valueOf( args[1].toUpperCase() );
            final GameMap gameMap = (GameMap) session.getGameMap();

            gameMap.getTeamLocationMap().put( gameTeam, SerializableLocation.getSerializedLocation( ( (Player) commandSender ).getLocation() ) );
            commandSender.sendMessage( "Added spawn for team " + gameTeam.name() );
            return true;
        } else if ( args[0].equalsIgnoreCase( "setBed" ) ) {
            if ( args.length != 3 ) {
                commandSender.sendMessage( "Korrekte Nutzung: /setup setBed <Team> <BlockFace>" );
                return true;
            }

            final GameTeam gameTeam = GameTeam.valueOf( args[1].toUpperCase() );
            final GameMap gameMap = (GameMap) session.getGameMap();
            final BlockFace blockFace = BlockFace.valueOf( args[2].toUpperCase() );

            final GameBed gameBed = new GameBed( gameTeam, blockFace,
                    SerializableLocation.getSerializedLocation( ( (Player) commandSender ).getLocation().getBlock().getLocation() ) );
            gameBed.setSession( session );

            gameBed.revive();

            gameMap.getTeamBedMap().put( gameTeam, gameBed );
            commandSender.sendMessage( "Added bed for team " + gameTeam.name() );
        }

        return false;
    }
}