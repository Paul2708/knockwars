package me.baluwii.knockwars.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import me.baluwii.knockwars.session.map.bed.IGameBed;
import me.baluwii.knockwars.session.team.GameTeam;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * JavaDoc this file!
 * Created: 05.08.2018
 *
 * @author Baluwii (paskutscha@gmail.com)
 */
@Getter
@RequiredArgsConstructor
public class BedReviveEvent extends Event {

    private final IGameBed gameBed;
    private final GameTeam gameTeam;

    private static final HandlerList handlers = new HandlerList();

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
